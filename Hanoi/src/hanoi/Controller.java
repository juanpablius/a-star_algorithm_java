/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hanoi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author juanp
 */
public class Controller {
    
    
    State initialState;
    State goalState;
    
    ArrayList <State> openList;
    ArrayList <State> closeList;
    
    ArrayList <State> solution;
    
    
    
    Controller ()
    {
        openList  = new ArrayList<>();
        closeList = new ArrayList<>();
        solution = new ArrayList<>();

    }
    
 
    
    public void run (int _nDisks)
    {
        //loadColumns (_nDisks)
        
        initialState = loadInitialState(_nDisks);
        goalState    = loadEndState (_nDisks);
        initialState.showActualState();
        
        solution = aStarAlgorithm();
        Collections.reverse(solution);
        printList(solution);
        

    }
    
   public State loadInitialState (int _nDisks){
        
        State estado;
        int f;
        ArrayList <Column> columns = new ArrayList<> ();
        
        Column c1 = new Column(_nDisks, 1);
        Column c2 = new Column(_nDisks, 2);
        Column c3 = new Column(_nDisks, 3);
        
        
        for (int i = _nDisks ; i > 0 ; i-- )
        {
            c1.addToColumn(i);
           
        }
        
        columns.add(c1);
        columns.add(c2);
        columns.add(c3);
        
              
        estado = new State(columns, _nDisks);
        estado.setFather(null);
        
        estado.setCost(0);
        estado.calculateHeuristic();
        
        f = estado.getCost() + estado.getHeuristic();
        estado.setEvaluationFunction(f);
        
       //estado.showActualState();
        return estado;
    }
   public State loadEndState(int _nDisks) 
    {
       State estado;
        ArrayList <Column> columns = new ArrayList<> ();
        
        Column c1 = new Column(_nDisks, 1);
        Column c2 = new Column(_nDisks, 2);
        Column c3 = new Column(_nDisks, 3);
        
        
        for (int i = _nDisks ; i > 0 ; i-- )
        {
            c3.addToColumn(i);
           
        }
        
        columns.add(c1);
        columns.add(c2);
        columns.add(c3);
        
        
        
        estado = new State(columns, _nDisks);
        
       //estado.showActualState();
        return estado;  
    }    

   public ArrayList <State> aStarAlgorithm ()
   {
       // Coger el primero de la lista de abiertos
      openList.add(initialState);
      State firstPosition;
      ArrayList <State> solution;
      State pointerState;
      boolean flag = false;
      boolean flag2 = false;
      int level = 0;
 

      while (openList.size()>0) 
      {
          firstPosition = openList.get(0);
          
          if (firstPosition.equalstoGoal(goalState))
          {
              
              solution = new ArrayList<>();
              solution.add(firstPosition);
              pointerState = firstPosition.getFather();
              
              while (pointerState != null)
              {
                  solution.add(pointerState);
                  pointerState = pointerState.getFather();
              }
              
              return solution;
          }
          
          
          
          firstPosition.expand();
          
          
          // This method is gonna compare each son state with the states in the openList and Close List
          // Son may have got a new g(n)
          // If this g(n) is better than OpenList/CloseList state then it'll be substituide
          firstPosition.workWithLists(openList, closeList);
          
          
          closeList.add(firstPosition);
          openList.remove(firstPosition);
          
          Collections.sort(openList);
          level++;
          
          System.out.println("Level " + level);
          System.out.println("Open List");
          printList(openList);
          System.out.println("Close List");
          printList(closeList);
          
          System.out.println("");
          System.out.println("");
         
          
      }// while*/
       
     return null;
       
       
   } 
     
   
    public void printList(ArrayList<State> _openList)
    {
   
        for (int i = 0; i <  _openList.size() ; i++) printState(_openList.get(i));
       
    }


    public void printState (State _state)
    {
        /*Show actual columns*/
        for (int i = 0; i < _state.columns.size(); i++)
            _state.columns.get(i).showColumn();
        System.out.println("g(n) = " + _state.getCost() + "  h(n) = " +  _state.getHeuristic() +" f(n) " + _state.getEvaluationFunction());
        System.out.println("--------------------------------------------------------------------");
    }    


    
    
}
