/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hanoi;

import java.util.ArrayList;

/**
 *
 * @author juanp
 */
public class Column {
    
    ArrayList <Integer> aColumn;
    int id; 
    
    public Column (int _numDisks, int _id)
    {
        aColumn = new ArrayList<> (_numDisks);
        id = _id;
    }
    
 
    
    
    public void showColumn ()
    {
        
        
        System.out.print ("[");
        for (int  i = 0; i < aColumn.size(); i++)   System.out.print((aColumn.get(i)) + " ");
        System.out.print ("]");
         
        System.out.println("");
    }
    
    
    
    public void addToColumn (int _disk)
    {
        aColumn.add(0,_disk);
    }
    
    public void addToList (int _disk)
    {
        aColumn.add(_disk);
    }
    
    public int getTop ()
    { 
        return aColumn.get(0);
            
    }
    
    public int getPosition (int j)
    {
        return (aColumn.get(j));
    }
    public void removeTop ()
    {
        aColumn.remove(0);
    }
    
    public boolean columnEmpty ()
    {
        return (aColumn.isEmpty());
    }
    
   // ArrayList<Integer> arrli = new ArrayList<Integer>(n); 
    
}
