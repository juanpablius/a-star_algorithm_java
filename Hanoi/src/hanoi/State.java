/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hanoi;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author juanp
 */
public class State  implements Comparable<State> {

 
    public ArrayList <Column> columns;
    public State father;
    ArrayList <State> sons;
    
    public int heuristic;
    public int cost;
    public Integer evaluationFunction;
    
    int disks;
   

    public State(ArrayList <Column> _columns, int _nDisks)
    {
        columns = _columns;
        father = null;
        sons = new ArrayList<>();
        disks = _nDisks;
        
    }
    
 
    public void showActualState ()
    {
        /*Show actual columns*/
        for (int i = 0; i < columns.size(); i++)
            columns.get(i).showColumn();
        System.out.println("g(n) = " + getCost() + "  h(n) = " +  getHeuristic() +" f(n) " + getEvaluationFunction());
        System.out.println("--------------------------------------------------------------------");
    }
    public void showStateParam (State _state)
    {
        /*Show actual columns*/
        for (int i = 0; i < _state.columns.size(); i++)
            _state.columns.get(i).showColumn();
        System.out.println("g(n) = " + _state.getCost() + "  h(n) = " +  _state.getHeuristic() +" f(n) " + _state.getEvaluationFunction());
        System.out.println("--------------------------------------------------------------------");
    }

    public void showSons ()
    {
        for (int i = 0; i < this.sons.size() ; i++) showStateParam(sons.get(i));
    }
        
    
    private boolean sameState(State a, State b) {
        
        if (a.getColumns().get(0).aColumn.size() == b.getColumns().get(0).aColumn.size())
        {
           if (a.getColumns().get(1).aColumn.size() == b.getColumns().get(1).aColumn.size())
           {
               if (a.getColumns().get(2).aColumn.size() == b.getColumns().get(2).aColumn.size())
               {
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < a.getColumns().get(i).aColumn.size(); j++)
                        {
                            if (!(a.getColumns().get(i).aColumn.get(j).equals(b.getColumns().get(i).aColumn.get(j))) )
                                             return false;
                        }
                    }
                   
               }
               else return false;
           }
            
           else return false; 
            
            
            
        }
        else return false;
        

       
        return true;
    }    
   

    
    public boolean secureState (int _origin, int _destination) 
    {
        /*
        A -> 0
        B -> 1 
        C -> 2
        */
       _origin--;
       _destination--;
        
        int topOrigen;
        int topDestination;
        
        if ( !(this.columns.get(_origin).columnEmpty()) ) // If the origin column is not empty
        {
            
            topOrigen = this.columns.get(_origin).getTop(); 
            
            if (this.columns.get(_destination).columnEmpty()) // If the destination is empty. The movement can be done
            {
                return true;
            }
            
            else
            {
                topDestination = this.columns.get(_destination).getTop();
                
                if (topDestination > topOrigen) return true; // If the Destination is less than the origin then true
                
            }
            
           
        }
        //If the origin column is  empty, the movement can't be done
        return false;
                
    }
    
    void expand() {

        
        /* Movements clockwise*/       
        movement (1,2,0); //moveLefttoMiddle
        movement (2,3,0);//moveMiddletoRight
        movement (3,1,0);//moveRighttoLeft
        /*Movements counter-clockwise*/
        movement (1,3,1); // MoveLeftToRight
        movement (3,2,1); // MoveRightToMiddle
        movement (2,1,1); // MoveMiddletoLeft

        
    }
        
    public void movement ( int _origin, int _desintation, int _clock)
    {
        State newState;
        int topOrigen; 
        int f;
        

         if (secureState(_origin, _desintation))
         {
              _origin--;
              _desintation--;
               
              /*Columns movements*/
              
              newState = this.createClone();
              
              topOrigen = newState.columns.get(_origin).getTop();
              
              newState.columns.get(_desintation).addToColumn(topOrigen);
              newState.columns.get(_origin).removeTop();
              
              /*Data know*/
              newState.calculateCost(_clock, topOrigen);
              newState.calculateHeuristic();
              
              f = newState.getCost() + newState.getHeuristic();
              
              newState.setEvaluationFunction(f);
              newState.setFather(this);
              
              if (!(compareFatherAndSon(newState))) sons.add(newState);
 
             
         }
         
         
        
    }
    
    
    public State createClone ()
    {
        State newState;
        ArrayList <Column> newColumns = new ArrayList<> ();
        
        Column _c1 = new Column(disks, 1); 
        Column _c2 = new Column(disks, 2);
        Column _c3 = new Column(disks, 3);
        
            
       for (int i = 0; i < this.getColumns().get(0).aColumn.size() ; i++)
        _c1.addToList(this.columns.get(0).getPosition(i)); 
       
       for (int i = 0; i < this.getColumns().get(1).aColumn.size() ; i++)
        _c2.addToList(this.columns.get(1).getPosition(i)); 
       
       for (int i = 0; i < this.getColumns().get(2).aColumn.size() ; i++)
        _c3.addToList(this.columns.get(2).getPosition(i)); 
        
 
      
        newColumns.add(_c1);
        newColumns.add(_c2);
        newColumns.add(_c3);
        
        
        
        newState = new State(newColumns, disks);
        newState.setCost(this.getCost());
        return newState;
    }
    

    
    public int calculateCost(int _clock, int topOrigen)  // g(n)
    {
       int parityDisks;
       int parityTop;
       int cost = 0;
       int totalCost = this.getCost();
       
       parityDisks = disks % 2;
       parityTop = topOrigen % 2;
             
       //Even Disks
              
       if (parityDisks == 0)
       {
           
           if (parityTop == 0) // Top Disk parity (Wise)
           {
               // Clockwise (Left to Middle, Middle to Right, Right to left
               if (_clock == 0) cost = 2;
              
               //Counter Clockwise (Left to Right, Right to Middle, middle to right
               if (_clock == 1) cost = 1;
               
           }
           
           else // Top Disk parity (Odd)
           {
               if (_clock == 0) cost = 1;
               if (_clock == 1) cost = 2;
           }
       }
       
       if (parityDisks != 0)
       {
           if (parityTop == 0)
           {
               if (_clock == 1) cost = 2;
               if (_clock == 0) cost = 1;
               
           }
           
           if (parityTop != 0)
           {
               if (_clock == 1) cost = 1;
               if (_clock == 0) cost = 2;
               
           }
       }
        
       totalCost = totalCost + cost;
       this.setCost(totalCost);
       
       return totalCost;
    
    }
    
    public int calculateHeuristic ()
    {
        int _heuristic;
        int rad = disks;
        int tam;
        int lastPositionOrdered;
        Column _column3;
        ArrayList <Integer> _aColumn;
        
        //Radio del mayor disco descolocado
         _column3 = columns.get(columns.size()-1);
        // _column3 = columns.get(0);
         _aColumn = _column3.aColumn; // Array Integers
         
         
         int j = 1;
         
         if (!(_aColumn.isEmpty()))
         {
             lastPositionOrdered = _aColumn.size() -j;
              
             for (int i = disks; i > 0; i--)
            {
                if (lastPositionOrdered < 0) break;
                
              //  System.out.println("Last Position  " + _aColumn.get(lastPositionOrdered) +  " i " + i);
                
                if ( i == _aColumn.get(lastPositionOrdered)) 
                {
                    rad--;
                    lastPositionOrdered--;
                
                }
                else break;                   
            }
         }
         
         
        // Collections.reverse(_aColumn);
         
        _heuristic =   ((int) Math.pow(2, rad)) - 1; // 2^r -1
        setHeuristic(_heuristic);
        return _heuristic;
        
    }
        
    
    void workWithLists(ArrayList<State> _openList, ArrayList<State> _closeList) 
    {
        int  flag = 0;
        int  flag2 = 0;
        for (int i = 0; i < this.sons.size(); i++) // For each son
        {
          
            
            /* Search in the close List*/
            for (int j = 0; j < _closeList.size() ; j++)
            {
               if (sameState (this.sons.get(i), _closeList.get(j)) ) 
               {
                  compareAndUpdateCost(_closeList.get(j), sons.get(i));                  
                  flag = 1;              // Do not search in the Open List
                  flag2 = 1;
                  j = _closeList.size(); // Exit this loop
                  
               }
            }
            
            /* Search in the open List*/
            for (int j = 0; (j < _openList.size()) && (flag == 0); j++)
            {
                
                if (sameState (this.sons.get(i), _openList.get(j)))
                {                 
                  compareAndUpdateCost(_openList.get(j), sons.get(i));
                  flag2 = 1;
                  flag = 1;              // Exit this loop
                    
                }               
                
            }
            
            /*Add to open List*/
            if ((flag == 0) && (flag2 == 0)) _openList.add(this.sons.get(i));
            
            /*Update Flags*/
            flag2 = 0;
            flag = 0;

        }
        

    }

    
    
    
    /*Encapsulated variables*/
        /**
     * @return the heuristic
     */
    public int getHeuristic() {
        return heuristic;
    }

    /**
     * @param heuristic the heuristic to set
     */
    public void setHeuristic(int heuristic) {
        this.heuristic = heuristic;
    }

    /**
     * @return the cost
     */
    public int getCost() {
        return cost;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(int cost) {
        this.cost = cost;
    }

    /**
     * @return the evaluationFunction
     */
    public int getEvaluationFunction() {
        return evaluationFunction;
    }

    /**
     * @param evaluationFunction the evaluationFunction to set
     */
    public void setEvaluationFunction(int evaluationFunction) {
        this.evaluationFunction = evaluationFunction;
    }
    
        /**
     * @return the father
     */
    public State getFather() {
        return father;
    }

    /**
     * @param father the father to set
     */
    public void setFather(State father) {
        this.father = father;
    }
    
    boolean compareFatherAndSon (State _son)
    {
        //Si no son iguales falso
        boolean value = false;
        
        if (this.getFather() == null)
        {
    
            return false;
        }
   
        if (sameState(this.getFather(), _son))  value = true;
         

        return (value);
        
    }

    boolean equalstoGoal(State goalState) 
    {       
        return (sameState(this, goalState));
    }
    
       /**
     * @return the columns
     */
    public ArrayList <Column> getColumns() {
        return columns;
    }

    /**
     * @param columns the columns to set
     */
    public void setColumns(ArrayList <Column> columns) {
        this.columns = columns;
    }

    public void compareAndUpdateCost(State stateAtList, State _state2)
    {
        if (_state2.getCost() < stateAtList.getCost())
            
        {
            stateAtList.setFather(_state2.getFather());
            stateAtList.setCost(_state2.getCost());
            stateAtList.setEvaluationFunction(_state2.getHeuristic());
        }
        else{
            _state2.setFather(stateAtList.getFather());
            _state2.setCost(stateAtList.getCost());
            _state2.setEvaluationFunction(stateAtList.evaluationFunction);
        }
        
    }

    @Override
    public int compareTo(State o) {
        return evaluationFunction.compareTo(o.evaluationFunction);
    }




    






    
    
}
